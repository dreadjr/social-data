var errorcodes = require('./errorcodes');


exports.getErrorResponse = function(errorcode){
    var err = {
        'status': errorcode,
        'message': errorcodes.getErrorMessage(errorcode)
    }
    return err ;
}