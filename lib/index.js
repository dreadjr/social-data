/**
 * Module dependencies.
 */
var SocialData = require('./social-data');


/**
 * Expose `SocialData` directly from package.
 */
exports = module.exports = SocialData;

/**
 * Export constructors.
 */
exports.SocialData = SocialData;
