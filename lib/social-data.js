var utils = require('./utils');
var errorcodes  = require('./errorcodes');

var socialData = function socialData(){
    var connectors = {};

    this.getData = function(serviceId, accessToken, datatype, extra, done) {
        var connector = connectors[serviceId] ;
        if( connector === undefined ){
            done(utils.getErrorResponse(errorcodes.CONTROLLER_NOT_REGISTERED),null);
        }

        connector.getData(accessToken, datatype, extra, done);
    }

    this.getSummaryData = function(serviceId, accessToken, datatype, extra, done) {
        var connector = connectors[serviceId] ;
        if( connector === undefined ){
            done(utils.getErrorResponse(errorcodes.CONTROLLER_NOT_REGISTERED),null);
        }
        connector.getSummaryData(accessToken, datatype, extra, done);
    }

    this.registerConnector = function(serviceId, connector) {
        if (typeof(connector.getServiceId) !== "function") {
            // safe to use the function
            return false ;
        }

        if (typeof(connector.getData) !== "function") {
            // safe to use the function
            return false ;
        }

        if (typeof(connector.getSummaryData) !== "function") {
            // safe to use the function
            return false ;
        }

        connectors[serviceId] = connector;
        return true ;
    }

    if(socialData.caller != socialData.getInstance){
        throw new Error("This object cannot be instantiated");
    }
}

/* ************************************************************************
 SINGLETON CLASS DEFINITION
 ************************************************************************ */
socialData.instance = null;

/**
 * Singleton getInstance definition
 * @return socialData class
 */
socialData.getInstance = function(){
    if(this.instance === null){
        this.instance = new socialData();
    }
    return this.instance;
}

module.exports = socialData.getInstance();

